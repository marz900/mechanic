/* eslint-disable react-native/no-inline-styles */
import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import COLORS from '../conts/colors';

const CustomRatingBar = () => {
  const [defaultRating, setdefaultRating] = React.useState(0);
  const [maxRating, setmaxRating] = React.useState([1, 2, 3, 4, 5]);

  const starImgFilled = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_filled.png';
  const starImgCorner = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_corner.png';

  // console.log('RRRRRRRRRRRRRR', defaultRating);
  return (
    <View style={{}}>
      <Text style={{color: COLORS.yellow}}>
        {maxRating.map((item, index) => {
          return (
            <TouchableOpacity activeOpacity={0.7} key={item} onPress={() => setdefaultRating(item)}>
              <Image
                style={{
                  width: 20,
                  height: 20,
                  resizeMode: 'cover',
                }}
                source={item <= defaultRating ? {uri: starImgFilled} : {uri: starImgCorner}}
              />
            </TouchableOpacity>
          );
        })}
      </Text>
    </View>
  );
};
export default CustomRatingBar;

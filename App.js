/* eslint-disable prettier/prettier */
import React from 'react';
import {LogBox} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import LoginScreen from './screens/LoginScreen';
import RegistrationScreen from './screens/RegistrationScreen';
import RegistrationCustomer from './screens/RegistrationCustomer';
import Loader from './components/Loader';
import AsyncStorage from '@react-native-async-storage/async-storage';

import HomeScreen from './screens/HomeScreen';
import ProfileScreen from './screens/ProfileScreen';
import CameraScreen from './screens/CameraScreen';
import MessageScreen from './screens/MessageScreen';
import OrderScreen from './screens/OrderScreen';
import MapViewScreen from './screens/MapViewScreen';
import TestData from './screens/TestData';
import OfferScreen from './screens/OfferScreen';
import OfferSentScreen from './screens/OfferSentScreen';
import ChatScreen from './screens/ChatScreen';
import UserGetOffer from './screens/UserGetOffer';
import UserSendOffer from './screens/UserSendOffer';

import Ionicons from 'react-native-vector-icons/Ionicons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import COLORS from './conts/colors';

import {AppProvider, AppContext} from './context/AppContext';

import { Provider } from 'react-redux';
import store from './screens/store/store';

const Tab = createBottomTabNavigator();
const MessageStack = createNativeStackNavigator();
const LogStack = createNativeStackNavigator();
const FeedStack = createNativeStackNavigator();

function FeedStackScreen() {
  return (
    <FeedStack.Navigator>
      <FeedStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <FeedStack.Screen
        name="OfferScreen"
        component={OfferScreen}
        options={{
          headerShown: false,
        }}
      />
      <FeedStack.Screen
        name="MapViewScreen"
        component={MapViewScreen}
        options={{
          headerShown: false,
        }}
      />
      <FeedStack.Screen
        name="OrderScreen"
        component={OrderScreen}
        options={{
          headerShown: false,
        }}
      />
      <FeedStack.Screen
        name="OfferSentScreen"
        component={OfferSentScreen}
        options={{
          headerShown: false,
        }}
      />
      <FeedStack.Screen
        name="UserGetOffer"
        component={UserGetOffer}
        options={{
          headerShown: false,
        }}
      />
      <FeedStack.Screen
        name="UserSendOffer"
        component={UserSendOffer}
        options={{
          headerShown: false,
        }}
      />
    </FeedStack.Navigator>
  );
}

const LogStackScreen = ({route}) => {
  return (
    <LogStack.Navigator>
      <LogStack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          headerShown: false,
        }}
      />
      <LogStack.Screen
        name="Home"
        component={HomeTabs}
        options={{
          headerShown: false,
        }}
      />
    </LogStack.Navigator>
  );
};

function MessageStackScreen({navigation, route}) {
  return (
    <MessageStack.Navigator>
      <MessageStack.Screen
        name="Message"
        component={MessageScreen}
        options={{
          headerShown: false,
        }}
      />
      <MessageStack.Screen
        name="ChatScreen"
        component={ChatScreen}
        options={({route}) => (
          console.log('first', route),
          {
            title: route.params.name,
            headerBackTitleVisible: false,
            headerShadowVisible: true,
            headerTransparent: true,
          }
        )}
      />
    </MessageStack.Navigator>
  );
}

function HomeTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {backgroundColor: COLORS.light, height: 50},
        tabBarInactiveTintColor: COLORS.black,
        tabBarActiveTintColor: COLORS.yellow,
      }}>
      <Tab.Screen
        name="HomeScreen"
        component={FeedStackScreen}
        options={{
          tabBarIcon: ({color, size}) => <Ionicons name="home-outline" color={color} size={30} />,
        }}
      />
      <Tab.Screen
        name="Message2"
        component={MessageStackScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="chat-processing-outline" color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="Camera"
        component={CameraScreen}
        options={{
           tabBarIcon: ({color, size}) => (
            <Feather name="camera" color={color} size={30} />
          ),
          // tabBarIcon: () => <CameraScreen />,
        }}
      />
      <Tab.Screen
        name="Orders"
        component={OrderScreen}
        options={{
          tabBarIcon: ({ color, size }) => <IconAntDesign name="profile" color={color} size={30} />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({color, size}) => <IconFontAwesome name="user-o" color={color} size={30} />,
        }}
      />
    </Tab.Navigator>
  );
}

const Stack = createNativeStackNavigator();

export default function App() {
  const [initialRouteName, setInitialRouteName] = React.useState('');

  React.useEffect(() => {
    setTimeout(() => {
      authUser();
    }, 2000);
  }, []);

  React.useEffect(() => {
    LogBox.ignoreAllLogs();
  }, []);

  const authUser = async () => {
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        userData = JSON.parse(userData);
        if (userData.loggedIn) {
          setInitialRouteName('HomeScreen');
        } else {
          setInitialRouteName('LoginScreen');
        }
      } else {
        setInitialRouteName('RegistrationScreen');
      }
    } catch (error) {
      setInitialRouteName('RegistrationScreen');
    }
  };

  return (
    // <AppProvider>
      
    // </AppProvider>
    <Provider store={store}>
      <AppProvider>
      <NavigationContainer>
        <>
          {!initialRouteName ? (
            <Loader visible={true} />
          ) : (
            <Stack.Navigator
              initialRouteName={initialRouteName}
              screenOptions={{headerShown: false}}>
              <Stack.Screen name="RegistrationScreen" component={RegistrationScreen} />
              <Stack.Screen name="RegistrationCustomer" component={RegistrationCustomer} />
              <Stack.Screen name="LoginScreen" component={LogStackScreen} />
              <Stack.Screen name="Home" component={HomeTabs} />
            </Stack.Navigator>
          )}
          </>
        </NavigationContainer>
        </AppProvider>
    </Provider>
  );
}

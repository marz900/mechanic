const data = [
  {
    id: 1,
    location: 'Moteur Pane',
    image:
      'https://previews.123rf.com/images/nopponpat/nopponpat1906/nopponpat190600046/134781307-mechanical-fixing-car-at-home-repairing-service-advice-by-mobile-phone-mechanic-technician-man-check.jpg',
  },
  {
    id: 2,
    location: 'Pneus pane',
    image: 'https://radio.vinci-autoroutes.com/medias/image/8e3dfba90c59fba908884ca774701659.jpg',
  },
  {
    id: 3,
    location: 'Car servicing',
    image: 'https://cdn.wearemarmalade.co.uk/wam/2020-12/car-service.jpg',
  },
  {
    id: 4,
    location: 'Car diagnostic',
    image: 'http://www.mot-testing-edinburgh.co.uk/data1/images/banner2.jpg',
  },
  {
    id: 5,
    location: 'Auto repair',
    image:
      'https://static.wixstatic.com/media/0c7dfc_4649e25b2a2640ff83c31528ccbbd933~mv2.jpg/v1/fill/w_1000,h_667,al_c,q_90,usm_0.66_1.00_0.01/0c7dfc_4649e25b2a2640ff83c31528ccbbd933~mv2.jpg',
  },
];

export default data;

const COLORS = {
  white: '#fff',
  black: '#000',
  blue: '#5D5FEE',
  grey: '#BABBC3',
  light: '#F3F4FB',
  darkBlue: '##7978B5',
  red: 'red',
  yellow: '#FFC300',
};

export default COLORS;

import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  ImageBackground,
  Alert,
  Button,
} from 'react-native';
import React from 'react';
import {useTheme} from 'react-native-paper';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {firebase} from '@react-native-firebase/auth';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import CustomRatingBar from '../rating/CustomRating';
import COLORS from '../conts/colors';
// import {AppContext} from '../context/AppContext';
import moment from 'moment';
// import DatePicker from 'react-datepicker';
import DatePicker from 'react-native-date-picker';

const UserSendOffer = ({navigation, route}) => {
  const [posts, setPosts] = React.useState(null);
  const [position, setPosition] = React.useState('');
  // const [startDate, setStartDate] = React.useState(new Date());
  const [price, setPrice] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [pending, setPanding] = React.useState('');
  const [date, setDate] = React.useState(new Date(Date.UTC(2012, 11, 20)));
  const [open, setOpen] = React.useState(false);
  const [dobLabel, setDobLabel] = React.useState('Select Date');
  const [postdetails, setPostDetails] = React.useState([])
  const {item} = route.params;
  const { colors } = useTheme();
  
React.useEffect(() => {
    console.log('object----', route.params);
   
}, [])
    const userNamePost = route.params.postdetails.userName
  const handleAdd = async () => {
    try {
      await firebase;
      firestore()
          .collection('UserOffer')
          .doc(auth().currentUser.uid)
        .set({
            userId: auth().currentUser.uid,
            userPostName: userNamePost,
          serviceId: id,
          post: name,
          postImg: image,
          price: price,
          description: description,
          position: position,
            date: dobLabel,
          postTime: firestore.FieldValue.serverTimestamp(),
        })
        .then(() => {
          console.log('Post Added!');
          Alert.alert('Post published!', 'Your post has been published Successfully!');
        })
        .catch(e => {
          console.log(e);
        });
    } catch (error) {
      console.log('first', error);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.userInfoSection, {}]}>
          <Image
            source={{uri: route.params.postdetails.postImg}}
            style={{
              width: '100%',
              height: 150,
              borderBottomRightRadius: 18,
              borderBottomLeftRadius: 18,
            }}
          />
        </View>
             
        <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
            <Title style={[styles.Item, {fontSize: 16}]}>{userNamePost}</Title>
            <Text>
              {' '}
              <CustomRatingBar />
            </Text>
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 16}]}>
              <IconMaterialIcons name="schedule" color={COLORS.yellow} size={20} />
              {'\t'}4:00 pm
            </Title>
          </View>
              </View>
              <View style={{

              }}>

        </View>
        <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
            <Title style={[styles.Item, {fontSize: 17}]}>Description</Title>
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 17}]}>
              <MaterialCommunityIcons name="hammer-wrench" color={COLORS.yellow} size={20} />
              {'\t'}
              {route.params.postdetails.repair}
            </Title>
          </View>
        </View>

        <View
          style={[
            styles.infoBoxWrapperDes,
            {
              borderWidth: 2,
              borderColor: COLORS.yellow,
              width: '80%',
              borderRadius: 10,
              alignSelf: 'center',
              shadowOpacity: 0.5,
              shadowOffset: {width: -2, height: 4},
            },
          ]}>
          <ScrollView>
            <TextInput
              style={{
                fontSize: 12,
                fontStyle: 'normal',
                textAlign: 'center',
              }}
              placeholder="Please describe your problem"
              autoCorrect={false}
              onChangeText={description => setDescription(description)}
            />
          </ScrollView>
              </View>
              
              <View style={{ paddingHorizontal: '8%',  top: '5%' }}>
                  <View style={styles.action1}>
                      <Text style={{alignItems: 'flex-start',}}>
                      Total Offer Price
                  </Text>
            <TextInput
              placeholder="500-50 000 USD"
              keyboardType="numeric"
              placeholderTextColor={COLORS.grey}
            autoCorrect={false}         
              onChangeText={price => setPrice(price)}
              style={[
                styles.textInput1,
                {
                    color: colors.text,
                    borderWidth: 1,
                    padding: 2,
                    textAlign: 'center',
                    width: '60%',
                    

                },
              ]}
                      />
              </View>
          <View style={styles.action}>
            <FontAwesome5 name="map-marker-alt" color={COLORS.yellow} size={20} />
            <TextInput
              placeholder="Downtown, Highway"
              placeholderTextColor={COLORS.grey}
              autoCorrect={false}
              value={route.params.postdetails.position}
              onChangeText={position => setPosition(position)}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <IconFontAwesome name="calendar-o" color={COLORS.yellow} size={20} />
            <TouchableOpacity onPress={() => setOpen(true)}>
              <Text style={{color: '#666', marginLeft: 5, marginTop: 5}}>{dobLabel}</Text>

              <DatePicker
                modal
                open={open}
                date={date}
                format="YYYY-MM-DD"
                placeholder="select date"
                mode={'date'}
                maximumDate={new Date('2005-01-01')}
                minimumDate={new Date('1980-01-01')}
                onConfirm={date => {
                  setOpen(false);
                  setDate(date);
                  setDobLabel(date.toLocaleDateString());
                }}
                onCancel={() => {
                  setOpen(false);
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <View style={{
        flex: 5,
        }}>
          <TouchableOpacity style={styles.commandButton} onPress={() => handleAdd()}>
          <Text style={styles.panelButtonTitle}>send offer</Text>
        </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
};
export default UserSendOffer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
    },
  textInput1: {
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
      color: COLORS.grey,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    height: '10%',
    marginVertical: '-2%',
  },
  infoBoxWrapperDes: {
    flexDirection: 'row',
    height: '12%',
    marginVertical: '-2%',
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '4%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '1%',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  commandButton: {
    padding: 13,
    borderRadius: 20,
    backgroundColor: COLORS.yellow,
    marginTop: 5,
    width: '50%',
    alignSelf: 'center',
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  action1: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey,
      paddingBottom: 1,
    justifyContent: 'space-evenly'
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey,
      paddingBottom: 1,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: COLORS.grey,
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
});

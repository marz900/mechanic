/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Alert, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import COLORS from '../conts/colors';

import Ionicons from 'react-native-vector-icons/Ionicons';

export default function CameraScreen({navigation}) {
  const [image, setImage] = useState(null);
  const [images, setImages] = useState(null);

  const pickSingleWithCamera = (cropping, mediaType = 'photo') => {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
      includeExif: true,
      mediaType,
    })
      .then(image => {
        console.log('received image', image.path);
        setImage({
          uri: image.path,
          width: image.width,
          height: image.height,
          mime: image.mime,
          mime: image.data,
        });
        setImages(null);
      })
      .catch(e => alert(e));
  };

  const pickSingleBase64 = cropit => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      includeBase64: true,
      includeExif: true,
    })
      .then(image => {
        console.log('received base64 image', image);
        setImage({
          uri: `data:${image.mime};base64,` + image.data,
          width: image.width,
          height: image.height,
        });
        setImages(null);
      })
      .catch(e => alert(e));
  };

  const renderAsset = image => {
    return renderImage(image);
  };

  const renderImage = image => {
    return (
      <ScrollView style={{padding: '10%'}}>
        <View style={{}}>
          <Image
            style={{width: 200, height: 200, resizeMode: 'contain', borderRadius: 30}}
            source={image}
          />
        </View>
      </ScrollView>
    );
  };

  return (
    <View style={styles.container}>
      
      <View style={[styles.container, {
      flexDirection: "column"
    }]}>
        <View style={{ flex: 1}}>
      </View>
        <View style={{ flex: 3, alignItems: "center", bottom: 30,}}>
        {image ? renderAsset(image) : null}
        {images ? images.map(i => <View key={i.uri}>{renderAsset(i)}</View>) : null}

      </View>
        <View style={{ flex: 3, }}>
          <View style={styles.selectImage}>
        <View>
          <TouchableOpacity style={styles.button} onPress={() => pickSingleWithCamera(true)}>
            <Text style={styles.buttonText}>
              <Ionicons name="camera-outline" color={COLORS.black} size={30} />
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{}}>
          <TouchableOpacity 
                style={{
                  justifyContent: 'center', 
                  alignItems: 'center',
                  borderWidth: 1,
                  padding: 10,
                  width: 300,
                  borderRadius: 20,
                  marginTop: 30
                }}
          onPress={() => pickSingleBase64(false)}
          >
            <Text style={styles.text}>
                select image
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      </View>
    </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  Uni: {
    width: '100%',
  },
  text: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
    borderColor: COLORS.yellow,
  },
  selectImage: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    width: '80%',
    padding: 15,
    borderRadius: 25,
    alignItems: 'center',
    marginTop: 40,
    borderWidth: 1,
  },
   buttonText: {
    color: COLORS.yellow,
    fontWeight: '700',
    fontSize: 16,
  },
  text: {
     color: COLORS.black,
    fontSize: 25,
     
   }
});

/* eslint-disable eqeqeq */
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Keyboard,
  Alert,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
  StatusBar,
  CheckBox
} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import {CommonActions} from '@react-navigation/native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import firestore, { firebase } from '@react-native-firebase/firestore';
import {SvgUri} from 'react-native-svg';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {AppContext} from '../context/AppContext';

import COLORS from '../conts/colors';
import Button from '../components/Button';
import Input from '../components/Input';
import Loader from '../components/Loader';

import IconAntDesign from 'react-native-vector-icons/AntDesign';

const {width} = Dimensions.get('screen')
const { height } = Dimensions.get('screen')

import {add} from '../screens/store/shudSlice'


import { useSelector, useDispatch } from 'react-redux'
import {user } from '../screens/store/shudSlice'

import RegistrationScreen from './RegistrationScreen';
import RegistrationCustomer from './RegistrationCustomer'

const LoginScreen = ({ navigation, route }) => {
  
  const [inputs, setInputs] = React.useState({email: '', password: ''});
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState([]);
  const [loggedIn, setloggedIn] = React.useState(false);
  const [userInfo, setUserInfo] = React.useState([]);
  const [authenticated, setAuthenticated] = React.useState(false);
  const [status, setStatus] = React.useState('customer');
  const { user, setUser } = React.useContext(AppContext);

  // const user = useSelector((state) => state.cart.user)
  const dispatch = useDispatch()


  React.useEffect(() => {
    //google sign in
    GoogleSignin.configure({
      scopes: ['email'],
      webClientId: '113436745739-f73ksji1nln4c65vvs7fpp9sjce9bpue.apps.googleusercontent.com',
      offlineAccess: true,
    });
    // console.log('datatatatatuser', user);
    
  }, [user]);

  React.useEffect(() => {
    //google sign in
    const unsubscribe = navigation.addListener('focus', () => {
    });
    console.log('objects6666', user);
    return unsubscribe;


  }, [navigation]);



  const signInWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const {accessToken, idToken} = await GoogleSignin.signIn();
      setloggedIn(true);
      const credential = auth.GoogleAuthProvider.credential(idToken, accessToken);
      const ret = await auth().signInWithCredential(credential);
      const user = ret.user;
      console.log('DATA-----', user.uid)
      setUser(user);
      if (user) {
        const payload = {
          uid: user.uid,
          email: user.email,
          name: user.displayName,
          photo: user.photoURL,
          postTime: firestore.FieldValue.serverTimestamp(),
        };
        console.log('CURRENTUSER', auth().currentUser.uid);
        firestore()
          .collection('Users')
          .doc(auth().currentUser.uid)
          .set(payload)
          .then(res => {
            if (res != 0) {
              navigation.navigate('Home', {
                screen: 'HomeScreen',
              });
            }
          })
          .catch(e => {
            console.log('ERRR', e);
          });
        console.log('pppp', res);
        if (user) setloggedIn(true);
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
      }
    }
  };

  //google sign in method with Email/Password
  const handleLogin = async () => {
    try {
      auth()
        .signInWithEmailAndPassword(inputs.email, inputs.password)
        .then(userCredentials => {
          const user = userCredentials.user;
          console.log('DATA-----111111111', user)
          setUser(user);
          if(user) {
            const payload = {
              email: user.email,
              name: user.displayName,
              photo: user.photoURL,
              postTime: firestore.FieldValue.serverTimestamp(),
            };
            console.log('CURRENTUSER', auth().currentUser.uid);
            navigation.navigate('Home', {
                    screen: 'HomeScreen',
                  });
          }
        }).catch((error) => {  Alert.alert(error.message); });
    } catch (error) {
      Alert.alert(error.code);
    }
  }
 const handleOnchange = (text, input) => {
    setInputs(prevState => ({...prevState, [input]: text}));
  };

  const handleError = (error, input) => {
    setErrors(prevState => ({...prevState, [input]: error}));
  };

  const validate = async () => {
    Keyboard.dismiss();
    let isValid = true;
    if (!inputs.email) {
      handleError('Please input email', 'email');
      isValid = false;
    }
    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    }
    if (isValid) {
      handleLogin();
    }
  };

  return (
    <SafeAreaView style={[styles.device,{backgroundColor: COLORS.white, flex: 1}]}>
      <Loader visible={loading} />
      <ScrollView contentContainerStyle={{paddingTop: StatusBar.currentHeight / 5}} persistentScrollbar={false} contentInsetAdjustmentBehavior='never scrollView' showsVerticalScrollIndicator={false} >
        <View style={[styles.view,{paddingHorizontal: 20}]}>
          <ImageBackground
            source={require('../assets/MC.png')}
            style={{
              height: 125,
              width: 125,
              alignSelf: 'center',
            }}
            imageStyle={{
              borderRadius: 15,
            }}
          />
          <View>
          <Text
            style={{color: COLORS.yellow, fontSize: 18, marginVertical: 10, textAlign: 'center'}}>
            Welcome to MC
          </Text>
          </View>
          <View>
          <Text
            style={{color: COLORS.black, fontSize: 12, marginVertical: 10, textAlign: 'center'}}>
            Mobile Mechanic Services
          </Text>
          </View>
          <View style={{marginTop: StatusBar.marginTop,}}>
            <View>
              <Input
                onChangeText={text => handleOnchange(text, 'email')}
                onFocus={() => handleError(null, 'email')}
                iconName="email-outline"
                label="Email"
                keyboardType='email-address'
                placeholder="Enter your email"
                error={errors.email}
              />
            </View>

            <View>
              <Input
                onChangeText={text => handleOnchange(text, 'password')}
                onFocus={() => handleError(null, 'password')}
                iconName="lock-outline"
                label="Password"
                placeholder="Enter your password"
                error={errors.password}
                password
              />
            </View>

            <View>
              <TouchableOpacity
                onPress={validate}
                style={{
                  backgroundColor: COLORS.black,
                  padding: '4%',
                  borderRadius: 25,
                  width: '80%',
                  alignSelf: 'center',
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: COLORS.light,
                  }}>
                  Sign In
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                alignSelf: 'center',
                flexDirection: 'row',
                bottom: '-5%',
              }}>
              <View style={[styles.icon, {}]}>
                <TouchableOpacity onPress={() => signInWithGoogle()}>
                  <Image
                    source={require('../assets/google.png')}
                    style={{
                      width: 45,
                      height: 45,
                    }}
                  />
                </TouchableOpacity>
              </View>
              <View style={[styles.icon, {}]}>
                <TouchableOpacity onPress={() => {}}>
                  <Image
                    source={require('../assets/pomme.png')}
                    style={{
                      width: 45,
                      height: 45,
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                paddingVertical: '10%',
              }}>
              <Text
                onPress={() => navigation.navigate('RegistrationScreen')}
                style={{
                  color: COLORS.black,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: 16,
                }}>
                Don't have an account?{' '}
                <Text style={{fontWeight: 'bold', color: COLORS.yellow, fontSize: 16}}>
                  Sign Up
                </Text>
              </Text>
                <View style={[styles.shad,{
                  justifyContent: 'space-around',
                  flexDirection: 'row',
                  marginTop: '2.5%'
                }]}>
                <Text style={[styles.choice,{}]} onPress={() => navigation.navigate('RegistrationScreen')}>
                  become a mechanic
                  </Text>
                <Text style={[styles.choice,{}]} onPress={() => navigation.navigate('RegistrationCustomer')}>
                  become a customer
                </Text>
                </View>
            </View>
          </View>
        </View>
        </ScrollView>
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  device: {
    width: width,
    height: height,
  },
  icon: {
    marginHorizontal: '4%',
  },
  choice: {
    color: COLORS.white,
    borderColor: COLORS.yellow,
    borderWidth: 2,
    padding: '2%',
    fontWeight: '700',
    fontSize: 15,
    borderRadius: 10,
    backgroundColor: COLORS.yellow
  },
  shad: {
    
  }

});

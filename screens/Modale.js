import React, {useState} from 'react';
import {Alert, Modal, StyleSheet, Text, Pressable, View} from 'react-native';
import COLORS from '../conts/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';

const modalText = () => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        statusBarTranslucent={true}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyle}>Tire Problem</Text>
              </Pressable>
            </View>
            <View>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  console.log('OOOOOOOO');
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.textStyle}>Failed Transmission</Text>
              </Pressable>
            </View>
            <View>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyle}>Battery Down</Text>
              </Pressable>
            </View>
            <View>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyle}>Engine Heatup Up</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
      <Pressable style={[styles.button, styles.buttonOpen]} onPress={() => setModalVisible(true)}>
        <Text style={styles.textStyle1}>Filter Issues{'\t'}</Text>

        <Ionicons name="ios-chevron-down-circle" color={COLORS.black} style={{}} size={18} />
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 45,
    height: 230,
    width: 180,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    justifyContent: 'space-between',
  },
  button: {
    flexDirection: 'row',
  },
  textStyle: {
    color: COLORS.black,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textStyle1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
    color: COLORS.black,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    color: 'red',
  },
});

export default modalText;

const { createSlice } = require('@reduxjs/toolkit');

const initialStateValue = {
    data: [],
    user: null
}

const cartSlice = createSlice({
    name: 'cart',
    initialState: initialStateValue,
    reducers: {
        add(state, action) {
            state.push(action.payload);
        },
        remove(state, action) {
            return state.filter((item) => item.id !== action.payload);
        },
        user(state, action) {
            return state.push(action.payload);
        },
    },
});

export const { add, remove, getUser } = cartSlice.actions;
export default cartSlice.reducer;
import { configureStore } from '@reduxjs/toolkit';
import cartReducer from './shudSlice';
import productReducer from './userInfos'

const store = configureStore({
    reducer: {
        cart: cartReducer,
        user: productReducer
    },
});
console.log("object-----", store.getState())
export default store;
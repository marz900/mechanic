import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  ImageBackground,
} from 'react-native';
import React from 'react';
import {useTheme} from 'react-native-paper';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import CustomRatingBar from '../rating/CustomRating';
import COLORS from '../conts/colors';

const OfferScreen = ({route}) => {
  console.log('PPPPPP---', route.params);
  const [userDetails, setUserDetails] = React.useState();
  const {colors} = useTheme();
  React.useEffect(() => {
    getUserData();
  }, []);

  const getUserData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      setUserDetails(JSON.parse(userData));
    }
  };
  const {item} = route.params;
  console.log('first', route.params);
  let image = route.params.item.image;
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={[
          styles.userInfoSection,
          {
            flex: 1,
            borderWidth: 1,
            borderColor: COLORS.yellow,
            width: '95%',
            alignSelf: 'center',
            justifyContent: 'center',
          },
        ]}>
        <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
          <Image
            source={{uri: image}}
            style={{
              width: '50%',
              height: 100,
              borderRadius: 30,
              marginRight: '1%',
            }}
          />
          {/* <Text
            style={{
              fontSize: 12,
              fontWeight: 'bold',
              fontStyle: 'normal',
              textAlign: 'center',
              borderColor: COLORS.yellow,
              borderWidth: 1,
              width: '50%',
            }}>
            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur.
          </Text> */}
        </View>

        {/* <View
          style={[
            styles.infoBoxWrapperDes,
            {
              borderWidth: 2,
              borderColor: COLORS.yellow,
              width: '50%',
              borderRadius: 10,
              alignSelf: 'center',
              justifyContent: 'center',
              padding: '15%',
            },
          ]}>
          <Text
            style={{
              fontSize: 12,
              fontWeight: 'bold',
              fontStyle: 'normal',
              textAlign: 'center',
            }}>
            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur.
          </Text>
        </View> */}
      </View>
      <View style={styles.infoBoxWrapper}>
        <View style={[styles.infoBox, {}]}>
          <Title style={[styles.Item, {fontSize: 17}]}>Offer{'\t'} description</Title>
        </View>
      </View>

      <View
        style={[
          styles.infoBoxWrapperDes,
          {
            borderWidth: 2,
            borderColor: COLORS.yellow,
            width: '86%',
            height: '20%',
            borderRadius: 10,
            alignSelf: 'center',
          },
        ]}>
        <TextInput
          placeholderTextColor={COLORS.grey}
          autoCorrect={false}
          style={[
            styles.textInput,
            {
              color: colors.text,
            },
          ]}
          //   {...props}
        />
      </View>

      <View style={{paddingHorizontal: '8%', paddingVertical: '7%'}}>
        <View style={styles.action}>
          <FontAwesome5 name="map-marker-alt" color={COLORS.yellow} size={20} />
          <TextInput
            placeholder="Downtown, Highway"
            placeholderTextColor={COLORS.grey}
            autoCorrect={false}
            style={[
              styles.textInput,
              {
                color: colors.text,
              },
            ]}
          />
        </View>

        <View style={styles.action}>
          <IconFontAwesome name="calendar-o" color={COLORS.yellow} size={20} />
          <TextInput
            placeholder="10/05/2022"
            placeholderTextColor={COLORS.grey}
            autoCorrect={false}
            style={[
              styles.textInput,
              {
                color: colors.text,
              },
            ]}
          />
        </View>
      </View>
      <TouchableOpacity style={styles.commandButton} onPress={() => {}}>
        <Text style={styles.panelButtonTitle}>Send Offer</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
export default OfferScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    height: '10%',
    marginVertical: '-2%',
  },
  infoBoxWrapperDes: {
    height: '12%',
    marginVertical: '-2%',
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '4%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '1%',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  commandButton: {
    padding: 15,
    borderRadius: 20,
    backgroundColor: COLORS.yellow,
    alignItems: 'center',
    marginTop: 5,
    width: '50%',
    alignSelf: 'center',
    top: '-3%',
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 7,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey,
    paddingBottom: 1,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: COLORS.grey,
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -5,
    paddingLeft: 10,
    color: '#05375a',
  },
});

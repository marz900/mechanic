import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  ImageBackground,
  Alert,
  Button,
} from 'react-native';
import React from 'react';
import {useTheme} from 'react-native-paper';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {firebase} from '@react-native-firebase/auth';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import ActionButton from 'react-native-action-button';

import ImagePicker from 'react-native-image-crop-picker';

import {
  InputField,
  InputWrapper,
  AddImage,
  SubmitBtn,
  SubmitBtnText,
  StatusWrapper,
} from '../styles/AddPost';

import CustomRatingBar from '../rating/CustomRating';
import COLORS from '../conts/colors';
import {AppContext} from '../context/AppContext';
import moment from 'moment';
// import DatePicker from 'react-datepicker';
import DatePicker from 'react-native-date-picker';

import storage from '@react-native-firebase/storage';

const OfferScreen = ({navigation, route}) => {
  const [posts, setPosts] = React.useState(null);
  const [image, setImage] = React.useState(null);
  const [position, setPosition] = React.useState('');
  // const [startDate, setStartDate] = React.useState(new Date());
  const [price, setPrice] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [pending, setPanding] = React.useState('');
  const [repair, setRepair] = React.useState('');
  const [date, setDate] = React.useState(new Date(Date.UTC(2012, 11, 20)));
  const [open, setOpen] = React.useState(false);
  const [dobLabel, setDobLabel] = React.useState('Select Date');
  const [postdetails, setPostDetails] = React.useState([])
  const { colors } = useTheme();
  const {user, userinfos} = React.useContext(AppContext)
  

  React.useEffect(() => {
    getItemDetails()
    console.log("kkkkkkk", user);
    console.log("------kkkkkk-----", userinfos);
  }, []);

  const { item } = route.params;
  let images = route.params.item.postImg;
  let name = route.params.item.post;
  var postdate = route.params.item.date;
  let postdescription = route.params.item.description;
  let postposition = route.params.item.position;
  let postprice = route.params.item.price;
  let postrepair = route.params.item.repair;

  let id = route.params.item.id;

  const getItemDetails = async() => {
    try {
    const res = await firestore()
    .collection('services')
    .doc(item.id)
    .get()
    .then(documentSnapshot => {
        console.log('item exists: ', documentSnapshot.exists);

      if (documentSnapshot.exists) {
          setPostDetails(documentSnapshot.exists)
        console.log('Item data---=====-----: ', documentSnapshot.exists);
        }
    });
    } catch (error) {
      console.log(error);
  }
  }

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      width: 1200,
      height: 780,
      cropping: true,
    }).then((image) => {
      console.log(image);
      const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
      setImage(imageUri);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 1200,
      height: 780,
      cropping: true,
    }).then((image) => {
      console.log(image);
      const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
      setImage(imageUri);
    });
  };

  const submitPost = async () => {
    const imageUrl = await uploadImage();
    console.log('Image Url: ', imageUrl);
    console.log('Post: ', post);

    firestore()
    .collection('posts')
    .add({
      userId: user.uid,
      post: post,
      postImg: imageUrl,
      postTime: firestore.Timestamp.fromDate(new Date()),
      likes: null,
      comments: null,
    })
    .then(() => {
      console.log('Post Added!');
      Alert.alert(
        'Post published!',
        'Your post has been published Successfully!',
      );
      setPost(null);
    })
    .catch((error) => {
      console.log('Something went wrong with added post to firestore.', error);
    });
  }

  const SendData = () => {
    return (
      <ActionButton buttonColor={COLORS.yellow}>
        <ActionButton.Item
          buttonColor="#9b59b6"
          title="Take Photo"
          onPress={takePhotoFromCamera}>
          <Icon name="camera-outline" style={styles.actionButtonIcon} />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#3498db"
          title="Choose Photo"
          onPress={choosePhotoFromLibrary}>
          <Icon name="md-images-outline" style={styles.actionButtonIcon} />
        </ActionButton.Item>
      </ActionButton>
    )
    
  }


  const uploadImage = async () => {
    if( image == null ) {
      return null;
    }
    const uploadUri = image;
    let filename = uploadUri.substring(uploadUri.lastIndexOf('/') + 1);

    // Add timestamp to File Name
    const extension = filename.split('.').pop(); 
    const name = filename.split('.').slice(0, -1).join('.');
    filename = name + Date.now() + '.' + extension;

    setUploading(true);
    setTransferred(0);

    const storageRef = storage().ref(`photos/${filename}`);
    const task = storageRef.putFile(uploadUri);

    // Set transferred state
    task.on('state_changed', (taskSnapshot) => {
      console.log(
        `${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`,
      );

      setTransferred(
        Math.round(taskSnapshot.bytesTransferred / taskSnapshot.totalBytes) *
          100,
      );
    });

    try {
      await task;

      const url = await storageRef.getDownloadURL();

      setUploading(false);
      setImage(null);

      // Alert.alert(
      //   'Image uploaded!',
      //   'Your image has been uploaded to the Firebase Cloud Storage Successfully!',
      // );
      return url;

    } catch (e) {
      console.log(e);
      return null;
    }

  };
  
  const handleAdd = async () => {
    
    try {
      await firestore()
        .collection('services')
        .add({
          userId: auth().currentUser.uid,
          userName: userinfos.fullname,
          postImg: image,
          price: price,
          description: description,
          position: position,
          date: dobLabel,
          repair: repair,
          // nameMechanic: 
          postTime: firestore.FieldValue.serverTimestamp(),
        })
        .then(() => {
          navigation.push('HomeScreen')
          console.log('Post Added!');
          // Alert.alert('Post published!', 'Your post has been published Successfully!');
        })
        .catch(e => {
          console.log(e);
        });
    } catch (error) {
      console.log('first', error);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {
        postdetails != false ? (
        <>
        <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.userInfoSection, {}]}>
          <Image
            source={{uri: images}}
            style={{
              width: '100%',
              height: 150,
              borderBottomRightRadius: 18,
              borderBottomLeftRadius: 18,
            }}
                />
            
        </View>
          
        <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
            {/* <Title style={[styles.Item, {fontSize: 16}]}>{user.displayName}</Title> */}
            <Caption>
            </Caption>
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 16}]}>
              <IconMaterialIcons name="schedule" color={COLORS.yellow} size={20} />
              {/* {'\t'}4:00 pm */}
            </Title>
          </View>
              </View>
              <View style={styles.infoBoxWrapper}> 
                <View style={[styles.infoBox, {}]}>
            <Title style={[styles.Item, {fontSize: 17}]}>Description</Title>
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 17}]}>
              <MaterialCommunityIcons name="hammer-wrench" color={COLORS.yellow} size={20} />
              {'\t'}
              {postrepair}
            </Title>
          </View>
        </View>

        <View
          style={[
            styles.infoBoxWrapperDes,
            {
              borderWidth: 2,
              borderColor: COLORS.yellow,
              width: '80%',
              borderRadius: 10,
              alignSelf: 'center',
              shadowOpacity: 0.5,
              shadowOffset: {width: -2, height: 4},
            },
          ]}>
          <ScrollView showsVerticalScrollIndicator={false} scroll>
            <TextInput
              style={{
                fontSize: 12,
                fontStyle: 'normal',
                textAlign: 'center',
              }}
              placeholder="Please describe your problem"
              autoCorrect={false}
              value={postdescription}
              onChangeText={description => setDescription(description)}
            />
          </ScrollView>
        </View>
        <View style={{paddingHorizontal: '8%'}}>
          <View style={styles.action}>
            <FontAwesome5 name="map-marker-alt" color={COLORS.yellow} size={20} />
            <TextInput
              placeholder="Downtown, Highway"
              placeholderTextColor={COLORS.grey}
              autoCorrect={false}
              value={postposition}
              onChangeText={position => setPosition(position)}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <IconFontAwesome name="calendar-o" color={COLORS.yellow} size={20} />
            <TouchableOpacity onPress={() => setOpen(true)}>
              <Text style={{color: '#666', marginLeft: 5, marginTop: 5}}>{dobLabel}</Text>

              <DatePicker
                modal
                open={open}
                date={date}
                format="YYYY-MM-DD"
                placeholder="select date"
                mode={'date'}
                maximumDate={new Date('2005-01-01')}
                minimumDate={new Date('1980-01-01')}
                onConfirm={date => {
                  setOpen(false);
                  setDate(date);
                  setDobLabel(date.toLocaleDateString());
                }}
                onCancel={() => {
                  setOpen(false);
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.action}>
            <Ionicons name="pricetag-sharp" color={COLORS.yellow} size={20} />
            <TextInput
              placeholder="ex: 100 USD"
              keyboardType="numeric"
              placeholderTextColor={COLORS.grey}
                    autoCorrect={false}
                    value={postprice + "$"}
              onChangeText={price => setPrice(price)}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
            />
          </View>
        </View>
      </ScrollView>

      <View style={{
        flex: 5,
      }}>
        <TouchableOpacity style={styles.commandButton} onPress={() => {}}>
          <Text style={styles.panelButtonTitle}>update item</Text>
        </TouchableOpacity>
        </View>
          </>
        )
          :
        
          (
            <>
        <ScrollView showsVerticalScrollIndicator={false}>
                <View style={[styles.userInfoSection, {}]}>
          <ImageBackground
            source={{uri: image}}
            style={{
              width: '100%',
              height: 150,
              borderBottomRightRadius: 18,
              borderBottomLeftRadius: 18,
            }}>
              <View style={{
                    justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                      {
                    image != undefined ? false : <Feather name="camera" color={COLORS.gray} size={50} />
                      
                  }

            </View>
            </ImageBackground>
                  
            
        </View>
                <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
                     
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 16}]}>
              {/* <IconMaterialIcons name="schedule" color={COLORS.yellow} size={20} /> */}
              {/* {'\t'}4:00 pm */}
            </Title>
          </View>
        </View>
        <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
            <Title style={[styles.Item, {fontSize: 17}]}>Description</Title>
          </View>
          <View style={styles.infoBox}>
                    {/* <MaterialCommunityIcons name="hammer-wrench" color={COLORS.yellow} size={20} /> */}
              <TextInput
                style={[
                styles.textInput,
                {
                  color: COLORS.gray,
                  borderWidth: 1,
                  borderColor: COLORS.yellow,
                  borderRadius: 10
                },
              ]}
              placeholder="type problem"
              autoCorrect={false}
              onChangeText={repair => setRepair(repair)}
            />
          </View>
        </View>

        <View
          style={[
            styles.infoBoxWrapperDes,
            {
              borderWidth: 2,
              borderColor: COLORS.yellow,
              width: '80%',
              borderRadius: 10,
              alignSelf: 'center',
              shadowOpacity: 0.5,
              shadowOffset: {width: -2, height: 4},
            },
          ]}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <TextInput
              style={{
                fontSize: 12,
                fontStyle: 'normal',
                textAlign: 'center',
              }}
              placeholder="Please describe your problem"
              autoCorrect={false}
              onChangeText={description => setDescription(description)}
            />
          </ScrollView>
        </View>
        <View style={{paddingHorizontal: '8%'}}>
          <View style={styles.action}>
            <FontAwesome5 name="map-marker-alt" color={COLORS.yellow} size={20} />
            <TextInput
              placeholder="Downtown, Highway"
              placeholderTextColor={COLORS.grey}
              autoCorrect={false}
              onChangeText={position => setPosition(position)}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
            />
          </View>

          <View style={styles.action}>
            <IconFontAwesome name="calendar-o" color={COLORS.yellow} size={20} />
            <TouchableOpacity onPress={() => setOpen(true)}>
              <Text style={{color: '#666', marginLeft: 5, marginTop: 5}}>{dobLabel}</Text>

              <DatePicker
                modal
                open={open}
                date={date}
                format="YYYY-MM-DD"
                placeholder="select date"
                mode={'date'}
                maximumDate={new Date('2005-01-01')}
                minimumDate={new Date('1980-01-01')}
                onConfirm={date => {
                  setOpen(false);
                  setDate(date);
                  setDobLabel(date.toLocaleDateString());
                }}
                onCancel={() => {
                  setOpen(false);
                }}
              />
            </TouchableOpacity>
          </View>
                
          <View style={styles.action}>
            <Ionicons name="pricetag-sharp" color={COLORS.yellow} size={20} />
            <TextInput
              placeholder="100 USD"
              keyboardType="numeric"
              placeholderTextColor={COLORS.grey}
              autoCorrect={false}
              onChangeText={price => setPrice(price)}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
            />
          </View>
                </View>
                
      </ScrollView>
      <SendData/>
      <View style={{
        flex: 5,
      }}>
        <TouchableOpacity style={styles.commandButton} onPress={() => handleAdd()}>
          <Text style={styles.panelButtonTitle}>add item</Text>
        </TouchableOpacity>
        </View>
      </>
      )
      }
    </SafeAreaView>
  );
};
export default OfferScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    height: '10%',
    marginVertical: '-2%',
  },
  infoBoxWrapperDes: {
    flexDirection: 'row',
    height: '12%',
    marginVertical: '-2%',
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '4%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '1%',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  commandButton: {
    padding: 13,
    borderRadius: 20,
    backgroundColor: COLORS.yellow,
    marginTop: 5,
    width: '50%',
    alignSelf: 'center',
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey,
    paddingBottom: 1,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: COLORS.grey,
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: COLORS.black,
    bottom: 5,
    width: '80%',
    padding: -2

  },
});

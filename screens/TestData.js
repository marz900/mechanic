import React, {useRef} from 'react';
import {View, Text, TextInput} from 'react-native';

const TestData = () => {
  const tasks = [
    {
      taskId: 1,
      taskName: 'Clean the bathroom',
      taskStatus: 'Complete',
    },
    {
      taskId: 2,
      taskName: 'Learn filtering data in React',
      taskStatus: 'To do',
    },
    {
      taskId: 3,
      taskName: 'Fix the bug on React project',
      taskStatus: 'To do',
    },
    {
      taskId: 4,
      taskName: 'Fix the car',
      taskStatus: 'Complete',
    },
  ];

  let searcString = 'project';
  const searchData = tasks.filter(task => task.taskName.includes(searcString));

  return (
    <View style={{backgroundColor: 'red', flex: 1}}>
      <Text>React Examples</Text>
      {searchData.map(item => {
        console.log('NNNNNNNNNN-----', item);
        return <Text key={item.taskId}>{item.taskName}</Text>;
      })}
    </View>
  );
};

export default TestData;

/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import COLORS from '../conts/colors';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ScrollView,
  TextInput,
  StyleSheet,
  FlatList,
} from 'react-native';
import {auth, firebase} from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {
  Container,
  Card,
  UserInfo,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  PostTime,
  MessageText,
  TextSection,
} from '../styles/MessageStyles';

import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {AppContext} from '../context/AppContext';

const MessageScreen = ({navigation}) => {
  const [data, getData] = useState([]);
  const { user, userinfos, setUserInfos } = React.useContext(AppContext);
  const [users, setUsers] = useState(null);

  React.useEffect(() => {
    getUsers();
    console.log('----------', userinfos)
  }, []);

  React.useEffect(() => {
    console.log(user.uid);
    console.log('Post Added!', user);
  }, []);


  const getUsers = async () => {
    let userStatus = userinfos.status
    if (userStatus != 'Mechanic') {
      try {
        const querySanp = await firestore().collection('Users').where("status", "!=", "Customer").get();
        // console.log('first000', querySanp)
        const allusers = querySanp.docs.map(docSnap => docSnap.data());
        console.log('fqiwufiefhiq', allusers)
        setUsers(allusers);
      } catch (e) { 
        console.log(e)
      }
    } else {
      const querySanp = await firestore().collection('Users').where("status", "!=", "Mechanic").get();
        // console.log('first000', querySanp)
        const allusers = querySanp.docs.map(docSnap => docSnap.data());
        console.log('fqiwufiefhiq', allusers)
        setUsers(allusers);
      }
      
    }
    

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.userInfoSection}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            padding: '5%',
          }}>
          <Text style={[styles.Uniform1, {}]}>Inbox</Text>
          <Text style={[styles.Uniform1, {}]} onPress={() => {}}>
            <SimpleLineIcons name="options-vertical" color={COLORS.light} size={18} />
          </Text>
        </View>
      </View>
      <Container>
        <FlatList
          data={users}
          keyExtractor={item => item.userId}
          renderItem={({item}) => (
            <Card
              onPress={() => navigation.navigate('ChatScreen', {name: item.fullname, uid: item.userId})}>
              <UserInfo>
                <UserImgWrapper>
                  <UserImg source={{uri: item.photoURL}} />
                </UserImgWrapper>
                <TextSection>
                  <UserInfoText>
                    <UserName>{item.fullname}</UserName>
                    <PostTime>{item.messageTime}</PostTime>
                  </UserInfoText>
                  <MessageText>{item.messageText}</MessageText>
                </TextSection>
              </UserInfo>
            </Card>
          )}
        />
      </Container>
    </SafeAreaView>
  );
};

export default MessageScreen;

const styles = StyleSheet.create({
  Uniform: {
    fontSize: 20,
    fontWeight: 'bold',
    fontStyle: 'normal',
    textTransform: 'capitalize',
    borderColor: COLORS.yellow,
    borderWidth: 1,
    width: '80%',
    textAlign: 'center',
    padding: '5%',
  },

  btn: {
    alignItems: 'center',
    flex: 1,
    bottom: '-2.5%',
  },
  Uniform1: {
    fontSize: 25,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '4%',
    color: COLORS.light,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  userInfoSection: {
    backgroundColor: COLORS.yellow,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
});

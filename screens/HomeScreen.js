/* eslint-disable no-dupe-keys */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable react-native/no-inline-styles */
import AsyncStorage from '@react-native-async-storage/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import React, {useState} from 'react';

import {auth, firebase} from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';


import Modal from 'react-native-modal';
import { useSelector } from 'react-redux';
import OfferScreen from './OfferScreen';

import Button from '../components/Button';
import {
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Alert,
  Pressable,
  StatusBar
} from 'react-native';
import {Avatar, Title, Text, List} from 'react-native-paper';
import {ScrollView} from 'react-native-virtualized-view';

import COLORS from '../conts/colors';

import CustomRatingBar from '../rating/CustomRating';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {AppContext} from '../context/AppContext';
import ModalTester from './Modale';
import ModalAll from './ModaleAll';

import { useDispatch } from 'react-redux';
import { add } from '../screens/store/shudSlice'

import Animated from 'react-native-reanimated';

import {
  InputField,
  InputWrapper,
  AddImage,
  SubmitBtn,
  SubmitBtnText,
  StatusWrapper,
} from '../styles/AddPost';
import { color } from 'native-base/lib/typescript/theme/styled-system';


const HomeScreen = ({navigation, route}) => {
  const [post, setPost] = useState(null);
  const [image, setImage] = useState(null);
  const [loading, setLoading] = useState(true);
  const [data, getData] = useState([]);
  const { user, userinfos, setUserInfos } = React.useContext(AppContext);
  
  React.useEffect(() => {
      getMarker();
    getUserDAta();
    // getUsers()
    console.log(user.uid);
  }, []);
  
  const getUserDAta = async() => {
    try {
    const res = await firestore()
    .collection('Users')
    .doc(user.uid)
    .get()
    .then(documentSnapshot => {
        console.log('User exists: ', documentSnapshot.exists);

      if (documentSnapshot.exists) {
          setUserInfos(documentSnapshot.data())
        console.log('User data: ', documentSnapshot.data());
        }
    });
    } catch (error) {
      console.log(error);
  }
}


  const getMarker = async () => {
    const userStatus = userinfos.status
    if (userStatus != 'Mechanic') {
      try {
      console.log('00011');
      const list = [];
      await firestore()
        .collection('services')
        .get().then(querySnapshot => {
          querySnapshot.forEach(doc => {
            const {post, postImg, date, description,position, price, repair} = doc.data();
            list.push({
              id: doc.id,
              ...doc.data(),
            });
          });
        });
      getData(list);
      if (loading) {
        setLoading(false);
      }
      } catch (error) {
        return error
    }
    } else {
      try {
      console.log('00011');
      const list = [];
      await firestore()
        .collection('services')
        .where("userId", "==", user.uid)
        .get().then(querySnapshot => {
          querySnapshot.forEach(doc => {
            const {post, postImg, date, description,position, price, repair} = doc.data();
            list.push({
              id: doc.id,
              ...doc.data(),
            });
          });
        });
      getData(list);
      if (loading) {
        setLoading(false);
      }

      } catch (error) {
        return error
    }
    }
  };

  let userStatus = userinfos.status

  
  const renderItem = ({ item, index }) => (
    <>
    <View>
      <TouchableOpacity onPress={() => userStatus != 'Customer' ? 
        navigation.navigate('OfferScreen', { item })
        :
        navigation.navigate('UserGetOffer', { item })
    }>
        <Image
          style={{
            height: 150,
            width: 150,
            margin: '4%',
            borderRadius: 10,
          }}
          source={{uri: item.postImg}}
        />
      </TouchableOpacity>
    </View>
    </>
  );

  const SendData = (item) => {
    return (
      <ActionButton buttonColor={COLORS.yellow} onPress={() => 
          navigation.navigate('OfferScreen', {item})
        }>
      </ActionButton>
    )
    
  }

  {
    if (userStatus != 'Mechanic') {
      return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
      }}>
      <StatusBar backgroundColor={COLORS.white} barStyle="dark-content" />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
            backgroundColor: COLORS.black
          }}>
          <TouchableOpacity>
            <Avatar.Image
            source={{
              uri: userinfos.photoURL,
            }}
            size={60}
          />
        </TouchableOpacity>
        <View style={{
          flexDirection: 'column',
          alignSelf: 'center',
          left: -60
          
          }}>
            <Text
              style={[
                
                styles.title,
                {
                  color: COLORS.yellow,
                },
              ]}>
              {userinfos.fullname}
            </Text>
              <CustomRatingBar />
            <View
            style={{top:10}}>
            <Text style={{
          fontSize: 10,
          color: COLORS.yellow
        }}>
            status: <Text style={{color: COLORS.white, fontSize: 10,}}>
              {userinfos.status}
          </Text>
        </Text>
              </View>
          </View>
        <View
          style={{
            flexDirection: 'column',
          }}>
          <TouchableOpacity onPress={() => { }}>
            <IconFontAwesome
              name="map-marker"
              style={{
                fontSize: 30,
                color: COLORS.yellow,
                padding: 12,
                borderRadius: 10,
                borderColor:COLORS.backgroundLight,
              }}
            />
        </TouchableOpacity>
          </View>
        
          </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          borderBottomWidth: 0.5,
        }}>
        <View
          style={[
            styles.row,
            {
              alignItems: 'center',
              paddingHorizontal: '5%',
            },
          ]}>
          <Text style={[styles.Uniform, {}]} onPress={() => {}}>
            All
            {'\t'}
          </Text>
        </View>
        <View>
          <Text
            style={[
              styles.Uniform2,
              {borderRightColor: '#000', borderRightWidth: 1, marginTop: 2},
            ]}>
            {'\t'}
          </Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.Uniform} onPress={() => navigation.push('MapViewScreen')}>
            <IconFontAwesome name="map-marker" color={COLORS.yellow} size={25} />
            {'\t'}Set Location
          </Text>
        </View>
        <View>
          <Text
            style={[
              styles.Uniform2,
              {borderRightColor: '#000', borderRightWidth: 1, marginTop: 2},
            ]}>
            {'\t'}
          </Text>
        </View>
        <View style={[styles.row, {alignItems: 'center'}]}>
          <Text style={[styles.Uniform, {}]} onPress={() => navigation.push('OrderScreen')}>
            <IconMaterialIcons name="schedule" color={COLORS.yellow} size={25} />
            {'\t'}<Text style={{
              alignItems: 'center'
            }}>
              Schedules
            </Text>
          </Text>
        </View>
      </View>

      <View
        style={{
          padding: '5%',
        }}>
        <View>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              fontStyle: 'normal',
            }}>
            Buyer Requests
          </Text>
        </View>
        <View
          style={{
            padding: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text>
            <ModalTester />
          </Text>
          <Text>
            <ModalAll />
          </Text>
        </View>
      </View>
      <ScrollView nestedScrollEnabled={true} showsHorizontalScrollIndicator={false}>
        <View style={{alignSelf: 'center'}}>
          <FlatList
            data={data}
            keyExtractor={item => item.id.toString()}
            renderItem={renderItem}
            numColumns={2}
          />
        </View>
      </ScrollView>
      </View>
  );
    } else {
      return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
      }}>
      <StatusBar backgroundColor={COLORS.white} barStyle="dark-content" />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
            backgroundColor: COLORS.black
          }}>
          <TouchableOpacity>
            <Avatar.Image
            source={{
              uri: userinfos.photoURL,
            }}
            size={60}
          />
        </TouchableOpacity>
        <View style={{
          flexDirection: 'column',
          alignSelf: 'center',
          left: -60
          
          }}>
            <Text
              style={[
                
                styles.title,
                {
                  color: COLORS.yellow,
                },
              ]}>
              {userinfos.fullname}
            </Text>
              <CustomRatingBar />
            <View
            style={{top:10}}>
            <Text style={{
          fontSize: 10,
          color: COLORS.yellow
        }}>
            status: <Text style={{color: COLORS.white, fontSize: 10,}}>
              {userinfos.status}
          </Text>
        </Text>
              </View>
          </View>
        <View
          style={{
            flexDirection: 'column',
          }}>
          <TouchableOpacity onPress={() => { }}>
            <IconFontAwesome
              name="map-marker"
              style={{
                fontSize: 30,
                color: COLORS.yellow,
                padding: 12,
                borderRadius: 10,
                borderColor:COLORS.backgroundLight,
              }}
            />
        </TouchableOpacity>
          </View>
        
          </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          borderBottomWidth: 0.5,
        }}>
        <View
          style={[
            styles.row,
            {
              alignItems: 'center',
              paddingHorizontal: '5%',
            },
          ]}>
          <Text style={[styles.Uniform, {}]} onPress={() => {}}>
            All
            {'\t'}
          </Text>
        </View>
        <View>
          <Text
            style={[
              styles.Uniform2,
              {borderRightColor: '#000', borderRightWidth: 1, marginTop: 2},
            ]}>
            {'\t'}
          </Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.Uniform} onPress={() => navigation.push('MapViewScreen')}>
            <IconFontAwesome name="map-marker" color={COLORS.yellow} size={25} />
            {'\t'}Set Location
          </Text>
        </View>
        <View>
          <Text
            style={[
              styles.Uniform2,
              {borderRightColor: '#000', borderRightWidth: 1, marginTop: 2},
            ]}>
            {'\t'}
          </Text>
        </View>
        <View style={[styles.row, {alignItems: 'center'}]}>
          <Text style={[styles.Uniform, {}]} onPress={() => navigation.push('OrderScreen')}>
            <IconMaterialIcons name="schedule" color={COLORS.yellow} size={25} />
            {'\t'}<Text style={{
              alignItems: 'center'
            }}>
              Schedules
            </Text>
          </Text>
        </View>
      </View>

      <View
        style={{
          padding: '5%',
        }}>
        <View>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              fontStyle: 'normal',
            }}>
            Buyer Requests
          </Text>
        </View>
        <View
          style={{
            padding: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text>
            <ModalTester />
          </Text>
          <Text>
            <ModalAll />
          </Text>
        </View>
      </View>
      <ScrollView nestedScrollEnabled={true} showsHorizontalScrollIndicator={false}>
        <View style={{alignSelf: 'center'}}>
          <FlatList
            data={data}
            keyExtractor={item => item.id.toString()}
            renderItem={renderItem}
            numColumns={2}
          />
        </View>
      </ScrollView>
      <SendData />
        </View>
  );
    }
}
  
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: '8%',
    marginBottom: 10,
    backgroundColor: COLORS.black,
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
  },
  Uniform2: {
    fontSize: 25,
    fontWeight: 'bold',
    fontStyle: 'normal',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 45,
    height: 230,
    width: 180,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    justifyContent: 'space-evenly',
  },
  button: {
    borderRadius: 20,
    padding: 5,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  // buttonClose: {
  //   backgroundColor: '#2196F3',
  // },
  textStyle: {
    color: COLORS.black,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 13,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    color: 'red',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

/* eslint-disable react-native/no-inline-styles */
// import AsyncStorage from '@react-native-async-storage/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {GoogleSignin} from '@react-native-community/google-signin';
import firestore from '@react-native-firebase/firestore';
import {firebase, auth} from '@react-native-firebase/auth';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  Keyboard,
  Alert,
  Dimensions,
  StatusBar
} from 'react-native';

// import {AppProvider, AppContext} from '../context/AppContext';

const {width} = Dimensions.get('screen')
const {height} = Dimensions.get('screen')

import Input from '../components/Input';
import COLORS from '../conts/colors';
import Loader from '../components/Loader';

const RegistrationCustomer = ({navigation, route}) => {
  const [post, setPost] = React.useState(null);
  const [inputsnull, setInputNull] = React.useState(null);
  const [inputs, setInputs] = React.useState({
    email: '',
    fullname: '',
    password: '',
    repassword: '',
  });
  // const {user} = React.useContext(AppContext);
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
     const unsubscribe = navigation.addListener('focus', () => {
    //   handleSignUp();
    }); 
    return unsubscribe;
    
  }, [navigation]);

  const validate = () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!inputs.email) {
      handleError('Please input email', 'email');
      isValid = false;
    } else if (!inputs.email.match(/\S+@\S+\.\S+/)) {
      handleError('Please input a valid email', 'email');
      isValid = false;
    }

    if (!inputs.fullname) {
      handleError('Please input fullname', 'fullname');
      isValid = false;
    }

    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    } else if (inputs.password.length < 5) {
      handleError('Min password length of 5', 'password');
      isValid = false;
    }
    if (!inputs.repassword) {
      handleError('Please input password', 'repassword');
      isValid = false;
    } else if (inputs.repassword.length < 5) {
      handleError('Min password length of 5', 'repassword');
      isValid = false;
    }

    if (isValid) {
      handleSignUp();
    }
  };

  const handleSignUp = async () => {
    try {
      const userCreate = await firebase
        .auth()
        .createUserWithEmailAndPassword(inputs.email, inputs.password);
      firestore()
        .collection('Users')
        .doc(userCreate.user.uid)
        .set({
          userId: userCreate.user.uid,
          fullname: inputs.fullname,
          email: inputs.email,
          photoURL: null,
          status: 'Customer',
          postTime: firestore.FieldValue.serverTimestamp(),
        })
        .then(() => {
          navigation.navigate('LoginScreen');
          console.log('Post Added!!!!!!');
          setPost(null);
        });
    } catch (error) {
      console.log('first', error);
    }
    // firebase
    //   .auth()
    //   .createUserWithEmailAndPassword(inputs.email, inputs.password)
    //   .then(userId => {
    //     if (firebase.auth().currentUser) {
    //       userId = firebase.auth().currentUser.uid;
    //       console.log('--------USERID----', userId);
    //       if (userId) {

    //       }
    //     }
    //   })
    //   .catch(error => {
    //     if (error.code === 'auth/email-already-in-use') {
    //       Alert.alert('email address is already in use!');
    //       // setInputs(null);
    //     }

    //     if (error.code === 'auth/invalid-email') {
    //       Alert.alert('That email address is invalid!');
    //     }
    //   });

    // .then(userCredentials => {
    //   const user = userCredentials.user;
    //   if (user != '') {
    //     console.log('----00000-----', user);
    //     navigation.navigate('LoginScreen');
    //   }
    // })
    // .catch(error => {
    //   if (error.code === 'auth/email-already-in-use') {
    //     Alert.alert('email address is already in use!');
    //   }

    //   if (error.code === 'auth/invalid-email') {
    //     Alert.alert('That email address is invalid!');
    //   }
    // });
  };

  const handleOnchange = (text, input) => {
    setInputs(prevState => ({...prevState, [input]: text}));
  };
  const handleError = (error, input) => {
    setErrors(prevState => ({...prevState, [input]: error}));
  };
  return (
    <SafeAreaView style={[styles.device, {backgroundColor: COLORS.white, flex: 1}]}>
      <Loader visible={loading} />
      <ScrollView
        contentContainerStyle={[{paddingVertical: StatusBar.currentHeight / 5}]}
        contentInsetAdjustmentBehavior='never' showsVerticalScrollIndicator={false}>
          <View style={[styles.view,{paddingHorizontal: 20}]}>
          <ImageBackground
          source={require('../assets/MC.png')}
          style={{
            height: 125,
            width: 125,
            alignSelf: 'center',
          }}
          imageStyle={{
            borderRadius: 15,
          }}
        />
          <Input
            onChangeText={text => handleOnchange(text, 'fullname')}
            onFocus={() => handleError(null, 'fullname')}
            value={post}
            iconName="account-outline"
            label="Full Name"
            placeholder="Enter your full name"
            error={errors.fullname}
          />

          <Input
            onChangeText={text => handleOnchange(text, 'email')}
            onFocus={() => handleError(null, 'email')}
            value={post}
            iconName="email-outline"
            label="Email"
            keyboardType='email-address'
            placeholder="Enter your email address"
            error={errors.email}
          />

          <Input
            onChangeText={text => handleOnchange(text, 'password')}
            onFocus={() => handleError(null, 'password')}
            value={post}
            iconName="lock-outline"
            label="Password"
            placeholder="password"
            error={errors.password}
            password
          />
          <Input
            onChangeText={text => handleOnchange(text, 'repassword')}
            onFocus={() => handleError(null, 'repassword')}
            value={post}
            iconName="lock-outline"
            label="Confirme password"
            placeholder="Re-type password"
            error={errors.repassword}
            password
          />
          <TouchableOpacity
            onPress={validate}
            style={{
              backgroundColor: COLORS.black,
              padding: '4%',
              borderRadius: 25,
              width: '80%',
              alignSelf: 'center',
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
                fontWeight: 'bold',
                color: COLORS.light,
              }}>
              Sign Up
            </Text>
          </TouchableOpacity>
          <View style={{}}>
            <Text
              onPress={() => navigation.navigate('LoginScreen')}
              style={{
                color: COLORS.black,
                fontWeight: 'bold',
                textAlign: 'center',
                fontSize: 16,
                paddingVertical: '5%',
              }}>
              Already have an account?{' '}
              <Text
                style={{
                  fontWeight: 'bold',
                  color: COLORS.yellow,
                  fontSize: 16,
                }}>
                Sign In
              </Text>
            </Text>
        </View>
          </View>
        
      </ScrollView>
    </SafeAreaView>
  );
};

export default RegistrationCustomer;
const styles = StyleSheet.create({
  device: {
    width: width,
    height: height,
  },
  view: { 
  }
});

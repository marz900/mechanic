import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  ImageBackground,
  Alert,
  Button,
} from 'react-native';
import React from 'react';
import {useTheme} from 'react-native-paper';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {firebase} from '@react-native-firebase/auth';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import CustomRatingBar from '../rating/CustomRating';
import COLORS from '../conts/colors';
// import {AppContext} from '../context/AppContext';
import moment from 'moment';
// import DatePicker from 'react-datepicker';
import DatePicker from 'react-native-date-picker';

const UserGetOffer = ({navigation, route}) => {
  const [postdetails, setPostDetails] = React.useState([])
  const {item} = route.params;
  const { colors } = useTheme();
  

  React.useEffect(() => {
      getItemDetails()
    console.log('-------*******', item);
  }, []);

    const getItemDetails = async() => {
    try {
    await firestore()
    .collection('services')
    .doc(item.id)
    .get()
    .then(documentSnapshot => {
        console.log('item exists: ', documentSnapshot.exists);

      if (documentSnapshot.exists) {
          setPostDetails(documentSnapshot.data())
        console.log('Item data---=====-----: ', documentSnapshot.data());
        }
    });
    } catch (error) {
      console.log(error);
  }
}

  const handleAdd = () => {
    navigation.navigate('UserSendOffer', {postdetails})
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.userInfoSection, {}]}>
          <Image
            source={{uri: postdetails.postImg}}
            style={{
              width: '100%',
              height: 150,
              borderBottomRightRadius: 18,
              borderBottomLeftRadius: 18,
            }}
          />
        </View>

        <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
            <Title style={[styles.Item, {fontSize: 16}]}>{postdetails.userName}</Title>
            <Caption>
              {' '}
              <CustomRatingBar />
            </Caption>
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 16}]}>
              <IconMaterialIcons name="schedule" color={COLORS.yellow} size={20} />
              {'\t'} 4:00 pm
            </Title>
          </View>
        </View>
        <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {}]}>
            <Title style={[styles.Item, {fontSize: 17}]}>Description</Title>
          </View>
          <View style={styles.infoBox}>
            <Title style={[styles.Item, {fontSize: 17}]}>
              <MaterialCommunityIcons name="hammer-wrench" color={COLORS.yellow} size={20} />
              {'\t'}
              {postdetails.repair}
            </Title>
          </View>
        </View>

        <View
          style={[
            styles.infoBoxWrapperDes,
            {
              borderWidth: 2,
              borderColor: COLORS.yellow,
              width: '80%',
              borderRadius: 10,
              alignSelf: 'center',
              shadowOpacity: 0.5,
              shadowOffset: {width: -2, height: 4},
            },
          ]}>
          <ScrollView>
            <Text style={[styles.getText, { textAlign: 'center',}]}>
              {postdetails.description}
            </Text>
          </ScrollView>
        </View>
        <View style={{paddingHorizontal: '8%'}}>
          <View style={styles.action}>
            <IconFontAwesome name="map-marker" color={COLORS.yellow} size={20} />
            <Text style={styles.getText}>
              {postdetails.position}
            </Text>
          </View>

          <View style={styles.action}>
            <IconFontAwesome name="calendar-o" color={COLORS.yellow} size={20} />
            <Text style={styles.getText}>
              {postdetails.date}
            </Text>
          </View>

          <View style={styles.action}>
            <Ionicons name="pricetag-sharp" color={COLORS.yellow} size={20} />
            <Text style={styles.getText}>
              {postdetails.price}$
            </Text>
          </View>
        </View>
      </ScrollView>
      <View style={{
        flex: 5,
        }}>
          <TouchableOpacity style={styles.commandButton} onPress={() => handleAdd()}>
          <Text style={styles.panelButtonTitle}>make offer</Text>
        </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
};
export default UserGetOffer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  getText: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingHorizontal: 10
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    height: '10%',
    marginVertical: '-2%',
  },
  infoBoxWrapperDes: {
    flexDirection: 'row',
    height: '12%',
    marginVertical: '3%',
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '4%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '1%',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  commandButton: {
    padding: 13,
    borderRadius: 20,
    backgroundColor: COLORS.yellow,
    marginTop: 5,
    width: '50%',
    alignSelf: 'center',
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey,
    paddingBottom: 2,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: COLORS.grey,
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
});

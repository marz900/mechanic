/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import COLORS from '../conts/colors';
import {View, Text, TouchableOpacity, Image, SafeAreaView, StyleSheet} from 'react-native';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalTester from './Modale';

export default function OrderScreen({navigation}) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.userInfoSection}>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 15,
            justifyContent: 'center',
          }}>
          <View style={{marginLeft: 20}}>
            <Title
              style={[
                styles.title,
                {
                  marginTop: 15,
                  marginBottom: 5,
                  textAlign: 'center',
                },
              ]}>
              Manager orders
            </Title>
          </View>
        </View>
      </View>

      <View style={styles.infoBoxWrapper}>
        <View style={[styles.infoBox, {}]}>
          <Title onPress={() => {}}>scheduled</Title>
        </View>
        <View style={styles.infoBox}>
          <Title onPress={() => {}}>completed</Title>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
    backgroundColor: COLORS.yellow,
    padding: '5%',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: COLORS.white,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  infoBox: {
    width: '50%',
    borderColor: COLORS.yellow,
    borderWidth: 1,
    alignItems: 'center',
    padding: '3%',
    margin: '5%',
    flex: 1,
    bottom: '7%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
});

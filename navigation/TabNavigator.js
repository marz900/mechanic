import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import CameraScreen from '../screens/CameraScreen';
import MessageScreen from '../screens/MessageScreen';
import OrderScreen from '../screens/OrderScreen';
import MapViewScreen from '../screens/MapViewScreen';
import TestData from '../screens/TestData';
import OfferScreen from '../screens/OfferScreen';
import OfferSentScreen from '../screens/OfferSentScreen';
import ChatScreen from '../screens/ChatScreen';

import Ionicons from 'react-native-vector-icons/Ionicons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import COLORS from '../conts/colors';

const Tab = createBottomTabNavigator();

const HomeStack = createNativeStackNavigator();
const MessageStack = createNativeStackNavigator();

function HomeStackScreen({navigation}) {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home1"
        component={HomeStackScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="MapViewScreen"
        component={MapViewScreen}
        options={{
          headerBackTitleVisible: false,
          headerShadowVisible: true,
          headerTransparent: true,
        }}
      />
      <HomeStack.Screen
        name="OfferScreen"
        component={OfferScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OrderScreen"
        component={OrderScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OfferSentScreen"
        component={OfferSentScreen}
        options={{
          headerShown: false,
        }}
      />
    </HomeStack.Navigator>
  );
}

function MessageStackScreen({navigation}) {
  return (
    <MessageStack.Navigator>
      <MessageStack.Screen
        name="Message"
        component={MessageScreen}
        options={{
          headerShown: false,
        }}
      />
      <MessageStack.Screen
        name="ChatScreen"
        component={ChatScreen}
        options={({route}) => ({
          title: route.params.userName,
          headerBackTitleVisible: false,
          headerShadowVisible: true,
          headerTransparent: true,
        })}
      />
    </MessageStack.Navigator>
  );
}

function TabNavigators() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {backgroundColor: COLORS.light, height: 50},
        tabBarInactiveTintColor: COLORS.black,
        tabBarActiveTintColor: COLORS.yellow,
      }}>
      <Tab.Screen
        name="Home"
        component={TabNavigators}
        options={{
          tabBarIcon: ({color, size}) => <Ionicons name="home-outline" color={color} size={30} />,
        }}
      />
      <Tab.Screen
        name="Message2"
        component={MessageStackScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="chat-processing-outline" color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="Camera"
        component={CameraScreen}
        options={{
          tabBarIcon: () => <CameraScreen />,
        }}
      />
      <Tab.Screen
        name="Orders"
        component={OrderScreen}
        options={{
          tabBarIcon: ({color, size}) => <IconAntDesign name="profile" color={color} size={30} />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({color, size}) => <IconFontAwesome name="user-o" color={color} size={30} />,
        }}
      />
    </Tab.Navigator>
  );
}

export default TabNavigators;

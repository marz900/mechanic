import React from 'react';

const AppContext = React.createContext();

const AppProvider = ({children}) => {
  const [user, setUser] = React.useState([]);
  const [userinfos, setUserInfos] = React.useState([]);
  return (
    <AppContext.Provider
      value={{
        user,
        setUser,
        userinfos,
        setUserInfos
      }}>
      {children}
    </AppContext.Provider>
  );
};

export {AppContext, AppProvider};
